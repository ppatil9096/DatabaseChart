package com.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import oracle.jdbc.driver.OracleDriver;

public class DataAccessObject {
    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        } else {
            try {
                DriverManager.registerDriver(new OracleDriver());
                connection = DriverManager.getConnection("jdbc:oracle:thin:@10.10.10.89:1521:live", "AURUSPAY", "AURUSPAY");
            } catch (SQLException e) {
                System.out.println("SQLException in getConnection() :: " + e);
            }
            return connection;
        }
    }

}
