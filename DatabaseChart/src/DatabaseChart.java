import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.jdbc.JDBCPieDataset;

import oracle.jdbc.driver.OracleDriver;

@WebServlet("/DatabaseChart")
public class DatabaseChart extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection connection = null;
        try {
            DriverManager.registerDriver(new OracleDriver());
            connection = DriverManager.getConnection("jdbc:oracle:thin:@10.10.10.89:1521:live", "AURUSPAY", "AURUSPAY");
            JDBCPieDataset dataset = new JDBCPieDataset(connection);
            dataset.executeQuery("select NAME, EXP from EMPLOYEE5 order by EXP");
            JFreeChart chart = ChartFactory.createPieChart("Pie Chart", dataset, true, true, false);
            if (chart != null) {
                response.setContentType("image/png");
                OutputStream outputStream = response.getOutputStream();
                ChartUtilities.writeChartAsPNG(outputStream, chart, 450, 400);
            }
        } catch (SQLException e) {
            System.out.println("SQLException in doGet() :: " + e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    System.out.println("SQLException  in finally block of doGet() :: " + e);
                }
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
