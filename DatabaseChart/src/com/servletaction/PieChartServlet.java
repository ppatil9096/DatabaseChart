package com.servletaction;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.jdbc.JDBCPieDataset;

import com.DAO.DataAccessObject;

@WebServlet("/PieChartServlet")
public class PieChartServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection = null;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        connection = DataAccessObject.getConnection();
        JDBCPieDataset dataset = new JDBCPieDataset(connection);
        try {
            dataset.executeQuery("select SOURCE, PERCENTAGE from AIR_POLLUTION");
            JFreeChart chart = ChartFactory.createPieChart("Pollution Chart", dataset, true, true, false);
            if (chart != null) {
                chart.setBorderVisible(true);
                int width = 600;
                int height = 400;
                response.setContentType("image/png");
                OutputStream outputStream = response.getOutputStream();
                ChartUtilities.writeChartAsPNG(outputStream, chart, width, height);
            }
        } catch (SQLException e) {
            System.out.println("SQLException in doGet() :: " + e);
        }
    }
}
